package com.app.game.utils;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Http{

    private static final Http INSTANCE = new Http();

    private OkHttpClient client;

    private Http() {
        client = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .build();
    }

    public String executeGet(String url) throws Error {
        try {
            Request request = new Request.Builder()
                    .get()
                    .url(url)
                    .build();

            Response result = client.newCall(request).execute();
            String data = result.body().string();

            return data;
        } catch(Exception e) {
            throw new Error(0, "Http.Error - 0");
        }
    }

    public String executePost(String url, Map<String, String> params) throws Error {
        try {
            Request request = new Request.Builder()
                    .post(createBody(params))
                    .url(url)
                    .build();

            Response result = client.newCall(request).execute();
            String data = result.body().string();

            return data;
        } catch(Exception e) {
            throw new Error(0, "Http.Error - 0");
        }
    }

    public static RequestBody createBody(Map<String, String> params) {
        FormBody.Builder builder = new FormBody.Builder();
        if(params!=null) {
            for(Map.Entry<String, String> param : params.entrySet()) {
                builder.add(param.getKey(), param.getValue());
            }
        }

        return builder.build();
    }

    public static Http getInstance() {
        return INSTANCE;
    }

    public static class Error extends Exception{

        private int code;

        public Error(int code, String message) {
            super(message);
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }
}
