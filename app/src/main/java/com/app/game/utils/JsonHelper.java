package com.app.game.utils;

import com.google.gson.Gson;

public class JsonHelper{

    public static <T> T fromJson(String json, Class<T> tClass) {
        try {
            return new Gson().fromJson(json, tClass);
        }
        catch(Exception e) {
            Log.e("GsonHelper: fromJson: exception: "+e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public static String toJson(Object object) {
        try {
            return new Gson().toJson(object);
        }
        catch(Exception e) {
            Log.e("GsonHelper: toJson: exception: "+e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T fromJson(String json, Gson gson, Class<T> tClass) {
        try {
            return gson.fromJson(json, tClass);
        }
        catch(Exception e) {
            Log.e("GsonHelper: fromJson: exception: "+e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public static String toJson(Object object, Gson gson) {
        try {
            return gson.toJson(object);
        }
        catch(Exception e) {
            Log.e("GsonHelper: toJson: exception: "+e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

}
