package com.app.game.utils;

import android.content.res.Resources;
import android.util.TypedValue;

import org.greenrobot.eventbus.EventBus;

public class Utils{

    public static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch(Exception e) {
            Log.e("AppUtils: sleep: exception: "+e.getMessage());
        }
    }

    public static int dpToPx(Resources resources, float dp) {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics()));
    }

    public static int pxToDp(Resources resources, float px) {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, px, resources.getDisplayMetrics()));
    }

    public static double parseDouble(String sValue, double defaulValue) {
        try {
            return Double.parseDouble(sValue);
        } catch(Exception e) {
            return defaulValue;
        }
    }

    public static float parseFloat(String sValue, float defaulValue) {
        try {
            return (float) Double.parseDouble(sValue);
        } catch(Exception e) {
            return defaulValue;
        }
    }

    public static int parseInt(String sValue, int defaulValue) {
        try {
            return (int) Double.parseDouble(sValue);
        } catch(Exception e) {
            return defaulValue;
        }
    }

    public static void eventBusRegister(Object object) {
        if(object!=null && !EventBus.getDefault().isRegistered(object)) {
            EventBus.getDefault().register(object);
        }
    }

    public static void eventBusUnregister(Object object) {
        if(object!=null && EventBus.getDefault().isRegistered(object)) {
            EventBus.getDefault().unregister(object);
        }
    }
}