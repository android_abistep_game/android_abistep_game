package com.app.game.utils;

import android.text.TextUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

public class Log{

    private static final String TAG = "develop";
    private static final boolean ON = true;

    private static long time = 0;

    public static void timerStart() {
        time = System.currentTimeMillis();
    }

    public static void timerComplete(String label) {
        long dTime = System.currentTimeMillis()-time;

        w((!TextUtils.isEmpty(label) ? label : "")+"dTime = "+dTime);

        time = System.currentTimeMillis();
    }

    public static final void d(String text) {
        if(ON) {
            android.util.Log.d(TAG, text);
        }
    }

    public static final void i(String text) {
        if(ON) {
            android.util.Log.i(TAG, text);
        }
    }

    public static final void e(String text) {
        if(ON) {
            android.util.Log.e(TAG, text);
        }
    }

    public static final void w(String text) {
        if(ON) {
            android.util.Log.w(TAG, text);
        }
    }

    public static final void d(Object object) {
        if(ON) {
            android.util.Log.d(TAG, object.toString());
        }
    }

    public static final void i(Object object) {
        if(ON) {
            android.util.Log.i(TAG, object.toString());
        }
    }

    public static final void e(Object object) {
        if(ON) {
            android.util.Log.e(TAG, object.toString());
        }
    }

    public static final void w(Object object) {
        if(ON) {
            android.util.Log.w(TAG, object.toString());
        }
    }

    public static final void listD(List list) {
        if(ON) {
            android.util.Log.d(TAG, "---------- Log list Start ----------");

            if(list==null) {
                android.util.Log.d(TAG, "null");
            }
            else {
                android.util.Log.d(TAG, "size = "+list.size());
                for(Object object : list) {
                    android.util.Log.d(TAG, object.toString());
                }
            }

            android.util.Log.d(TAG, "---------- Log list End ----------");
        }
    }

    public static final void listI(List list) {
        if(ON) {
            android.util.Log.i(TAG, "---------- Log list Start ----------");

            if(list==null) {
                android.util.Log.i(TAG, "null");
            }
            else {
                android.util.Log.i(TAG, "size = "+list.size());
                for(Object object : list) {
                    android.util.Log.i(TAG, object.toString());
                }
            }

            android.util.Log.i(TAG, "---------- Log list End ----------");
        }
    }

    public static final void listE(List list) {
        if(ON) {
            android.util.Log.e(TAG, "---------- Log list Start ----------");

            if(list==null) {
                android.util.Log.e(TAG, "null");
            }
            else {
                android.util.Log.e(TAG, "size = "+list.size());
                for(Object object : list) {
                    android.util.Log.e(TAG, object.toString());
                }
            }

            android.util.Log.e(TAG, "---------- Log list End ----------");
        }
    }

    public static final void listW(List list) {
        if(ON) {
            android.util.Log.w(TAG, "---------- Log list Start ----------");

            if(list==null) {
                android.util.Log.w(TAG, "null");
            }
            else {
                android.util.Log.w(TAG, "size = "+list.size());
                for(Object object : list) {
                    android.util.Log.w(TAG, object.toString());
                }
            }

            android.util.Log.w(TAG, "---------- Log list End ----------");
        }
    }

    public static final <K, V> void mapD(Map<K, V> map) {
        if(ON) {
            android.util.Log.d(TAG, "---------- Log map Start ----------");

            if(map==null) {
                android.util.Log.d(TAG, "null");
            }
            else {
                android.util.Log.d(TAG, "size = "+map.size());
                for(Map.Entry<K, V> entry : map.entrySet()) {
                    android.util.Log.d(TAG, entry.getKey()+" : "+entry.getValue());
                }
            }

            android.util.Log.i(TAG, "---------- Log map End ----------");
        }
    }

    public static String objectToString(Object object) {
        return object==null ? "null" : object.toString();
    }

    public static String arraySize(byte[] array) {
        return array==null ? "null" : ""+array.length;
    }

    public static String listSize(List list) {
        return list==null ? "null" : ""+list.size();
    }

    public static String mapSize(Map map) {
        return map==null ? "null" : ""+map.size();
    }

    public static void ex(Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        Log.e(e.getMessage());
        Log.e(sw.toString());
    }

    public static void ex(String label, Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        Log.e(label+e.getMessage());
        Log.e(label+sw.toString());
    }

}
