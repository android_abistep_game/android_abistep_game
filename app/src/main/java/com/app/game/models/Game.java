package com.app.game.models;

import com.app.game.utils.Log;

public class Game{

    public String id;
    public String url;
    public String name;
    public String descr;
    public String order;

    public String roomID;

    public String imgLogo;
    public String ingPreview;
    public String imgFreeSpins;
    public String imgLogoBig;
    public String imgBrag;

    public long accessMaxPayout;
    public String accessFreeForAll;
    public String accessMinLevel;
    public String accessMinFriends;
    public String accessMinUnlocks;

    public String bonusGame;
    public String bonusGameImage;
    public String bonusGameDescription;

    public boolean promoTourney;
    public boolean promoIsMega;

    public long releaseDate;
    public boolean releaseNew;
    public boolean releaseComingSoon;

    public Game() {
    }

    @Override
    public String toString() {
        return "Game{"+
                "id='"+id+'\''+
                ", url='"+url+'\''+
                ", name='"+name+'\''+
                ", descr='"+descr+'\''+
                ", order='"+order+'\''+
                ", roomID='"+roomID+'\''+
                ", imgLogo='"+imgLogo+'\''+
                ", ingPreview='"+ingPreview+'\''+
                ", imgFreeSpins='"+imgFreeSpins+'\''+
                ", imgLogoBig='"+imgLogoBig+'\''+
                ", imgBrag='"+imgBrag+'\''+
                ", accessMaxPayout="+accessMaxPayout+
                ", accessFreeForAll='"+accessFreeForAll+'\''+
                ", accessMinLevel='"+accessMinLevel+'\''+
                ", accessMinFriends='"+accessMinFriends+'\''+
                ", accessMinUnlocks='"+accessMinUnlocks+'\''+
                ", bonusGame='"+bonusGame+'\''+
                ", bonusGameImage='"+bonusGameImage+'\''+
                ", bonusGameDescription='"+bonusGameDescription+'\''+
                ", promoTourney="+promoTourney+
                ", promoIsMega="+promoIsMega+
                ", releaseDate="+releaseDate+
                ", releaseNew="+releaseNew+
                ", releaseComingSoon="+releaseComingSoon+
                '}';
    }

    public String toStringMain() {
        return "Game{"+
                "id='"+id+'\''+
                ", url='"+url+'\''+
                ", name='"+name+'\''+
                ", descr='"+descr+'\''+
                ", order='"+order+'\''+
                ", roomID='"+roomID+'\''+
                '}';
    }

    public String toStringImages() {
        return "Game{"+
                "imgLogo='"+imgLogo+'\''+
                ", ingPreview='"+ingPreview+'\''+
                ", imgFreeSpins='"+imgFreeSpins+'\''+
                ", imgLogoBig='"+imgLogoBig+'\''+
                ", imgBrag='"+imgBrag+'\''+
                '}';
    }

    public String toStringAccess() {
        return "Game{"+
                "accessMaxPayout="+accessMaxPayout+
                ", accessFreeForAll='"+accessFreeForAll+'\''+
                ", accessMinLevel='"+accessMinLevel+'\''+
                ", accessMinFriends='"+accessMinFriends+'\''+
                ", accessMinUnlocks='"+accessMinUnlocks+'\''+
                '}';
    }

    public String toStringBonus() {
        return "Game{"+
                "bonusGame='"+bonusGame+'\''+
                ", bonusGameImage='"+bonusGameImage+'\''+
                ", bonusGameDescription='"+bonusGameDescription+'\''+
                '}';
    }

    public String toStringPromo() {
        return "Game{"+
                "promoTourney="+promoTourney+
                ", promoIsMega="+promoIsMega+
                '}';
    }

    public String toStringReliaze() {
        return "Game{"+
                "releaseDate="+releaseDate+
                ", releazeNew="+releaseNew+
                ", releazeComingSoon="+releaseComingSoon+
                '}';
    }

    public void log() {
        Log.d(toStringMain());
        Log.d(toStringImages());
        Log.d(toStringAccess());
        Log.d(toStringBonus());
        Log.d(toStringPromo());
        Log.d(toStringReliaze());
        Log.d("^^^^^^^^^^");
    }

}
