package com.app.game.models;

import com.app.game.utils.Log;

import java.util.ArrayList;
import java.util.List;

public class Room{

    public String id;
    public String name;
    public String background;
    public List<Game> games;

    public Room() {
        games = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Room{"+
                "id='"+id+'\''+
                ", name='"+name+'\''+
                ", background='"+background+'\''+
                ", games="+Log.listSize(games)+
                '}';
    }

    public void log() {
        Log.d(toString());
        Log.d("##########");

        if(games!=null) {
            for(Game game : games) {
                if(game!=null) {
                    game.log();
                } else {
                    Log.d("Game = NULL");
                }
            }
        } else {
            Log.d("Games = NULL");
        }
    }

}
