package com.app.game.providers.games;

import android.content.Context;

import com.app.game.models.Room;
import com.app.game.providers.Api;
import com.app.game.providers.LoadingSate;
import com.app.game.utils.Http;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class GamesProvider{

    private static final GamesProvider INSTANCE = new GamesProvider();

    private boolean loading;
    private MutableLiveData<Data> data;
    private PublishSubject<String> loader;

    private GamesProvider() {

        loading = false;

        data = new MutableLiveData<>();
        data.setValue(new Data(LoadingSate.NULL, new ArrayList<>(), Error.NO_ERRORS));

        loader = PublishSubject.create();
        loader.filter(s->!loading)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(s->loading = true)
                .doOnNext(s->data.setValue(new Data(LoadingSate.LOADING, new ArrayList<>(), Error.LOADING)))
                .observeOn(Schedulers.io())
                .map(s->Http.getInstance().executeGet(Api.BASE_URL+Api.METHOD_GET_GAMES))
                .map(data->GamesParser.parseRooms(data))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable->onError(throwable))
                .doOnError(throwable -> loading = false)
                .retry()
                .doOnNext(rooms -> loading = false)
                .subscribe(rooms->onResult(rooms));
    }

    public void init(Context context) {

    }

    public void refersh() {
        loader.onNext("refresh");
    }

    private void onResult(List<Room> rooms) {
        data.setValue(new Data(LoadingSate.SUCCESS, rooms, Error.NO_ERRORS));
    }

    private void onError(Throwable throwable) {
        if(throwable instanceof Http.Error) {
            data.setValue(new Data(LoadingSate.ERROR, new ArrayList<>(), Error.LOADING));
        } else if(throwable instanceof GamesParser.Error) {
            data.setValue(new Data(LoadingSate.ERROR, new ArrayList<>(), Error.PARSING));
        }
    }

    public LiveData<Data> getData() {
        return data;
    }

    public static GamesProvider getInstance() {
        return INSTANCE;
    }

    public enum Error{
        LOADING,
        PARSING,
        NO_ERRORS
    }

    public static class Data{

        public final LoadingSate state;
        public final List<Room> rooms;
        public final Error error;

        public Data(LoadingSate state, List<Room> rooms, Error error) {
            this.state = state;
            this.rooms = rooms;
            this.error = error;
        }
    }
}
