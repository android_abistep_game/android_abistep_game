package com.app.game.providers.user;

import android.content.Intent;

import com.app.game.utils.Log;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

public class AuthProvider implements FacebookCallback<LoginResult>{

    private static final AuthProvider INSTANCE = new AuthProvider();

    private CallbackManager callbackManager;

    private AuthProvider() {
    }

    public void init() {
        if(callbackManager==null) {
            callbackManager = CallbackManager.Factory.create();
            LoginManager.getInstance().registerCallback(callbackManager, this);
        }
    }

    @Override
    public void onSuccess(LoginResult loginResult) {

    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {

    }

    public boolean isAuth() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken!=null && !accessToken.isExpired();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void log() {
        Log.d("AuthProvider: isAuth() = "+isAuth());
    }

    public static AuthProvider getInstance() {
        return INSTANCE;
    }
}
