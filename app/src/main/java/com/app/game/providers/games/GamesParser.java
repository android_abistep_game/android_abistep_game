package com.app.game.providers.games;

import com.app.game.models.Game;
import com.app.game.models.Room;
import com.app.game.utils.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GamesParser{

    public static List<Room> parseRooms(String data) throws Error {
        List<Room> rooms = new ArrayList<>();

        try {
            JSONObject jsonRoot = new JSONArray(data).getJSONObject(0);
            JSONObject jsonRooms = jsonRoot.getJSONObject("Rooms");
            JSONArray jsonRoomsNames = jsonRooms.names();

            Log.i("GamesParser: parseRooms: jsonRooms = "+jsonRooms.length());
            Log.i("GamesParser: parseRooms: jsonRooms = "+jsonRooms.names());

            for(int i = 0; i<jsonRoomsNames.length(); i++) {
                String key = jsonRoomsNames.optString(i);
                JSONObject jsonRoom = jsonRooms.getJSONObject(key);

                Log.i("GamesParser: parseRooms: key["+i+"] = "+key);
                Log.i("GamesParser: parseRooms: jsonRoom["+i+"] = "+jsonRoom);

                Room room = parseRoom(jsonRoom);
                rooms.add(room);
                room.log();
            }
        } catch(Exception e) {
            e.printStackTrace();
            throw new Error(e.getMessage());
        }

        return rooms;
    }

    public static Room parseRoom(JSONObject jsonRoom) throws JSONException {
        Room room = new Room();
        room.id = jsonRoom.getString("RoomID");
        room.name = jsonRoom.getString("RoomName");
        room.background = jsonRoom.getString("BackgroundURL");
        room.games.addAll(parseGames(jsonRoom.getJSONArray("Games")));
        return room;
    }

    public static List<Game> parseGames(JSONArray jsonGames) throws JSONException {
        List<Game> games = new ArrayList<>();
        for(int i = 0; i<jsonGames.length(); i++) {
            JSONObject jsonGame = jsonGames.getJSONObject(i);
            Game game = parseGame(jsonGame);
            games.add(game);
        }
        return games;
    }

    public static Game parseGame(JSONObject jsonGame) throws JSONException {
        Game game = new Game();

        game.id = jsonGame.getString("GameID");
        game.url = jsonGame.getString("GameURLNew");
        game.name = jsonGame.getString("GameName");
        game.descr = jsonGame.getString("GameDescription");
        game.order = jsonGame.getString("SortOrder");

        game.roomID = jsonGame.getString("RoomID");

        game.imgLogo = jsonGame.getString("LogoURL");
        game.ingPreview = jsonGame.getString("PreviewURL");
        game.imgFreeSpins = jsonGame.getString("FreeSpinsImageURL");
        game.imgLogoBig = jsonGame.getString("BiggerGameLogoURL");
        game.imgBrag = jsonGame.getString("BragImageURL");

        game.accessMaxPayout = jsonGame.getLong("MaxPayout");
        game.accessFreeForAll = jsonGame.getString("FreeForAll");
        game.accessMinLevel = jsonGame.getString("MinLevelToPlay");
        game.accessMinFriends = jsonGame.getString("MinFriendsToPlay");
        game.accessMinUnlocks = jsonGame.getString("MinUnlocksToPlay");

        game.bonusGame = jsonGame.getString("BonusGameURL");
        game.bonusGameImage = jsonGame.getString("BonusGameImageURL");
        game.bonusGameDescription = jsonGame.getString("BonusGameDescription");

        game.promoTourney = jsonGame.getBoolean("TourneyPromo");
        game.promoIsMega = jsonGame.getBoolean("IsMegaPromo");

        game.releaseDate = jsonGame.getLong("ReleaseDate");
        game.releaseNew = jsonGame.getBoolean("New");
        game.releaseComingSoon = jsonGame.getBoolean("ComingSoon");

        return game;
    }

    public static class Error extends Exception{

        public Error() {
        }

        public Error(String message) {
            super(message);
        }

        public Error(String message, Throwable cause) {
            super(message, cause);
        }

        public Error(Throwable cause) {
            super(cause);
        }
    }
}
