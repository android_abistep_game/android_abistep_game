package com.app.game.providers.user;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class UserLevelProvider{

    private static final UserLevelProvider INSTANCE = new UserLevelProvider();

    private MutableLiveData<Integer> levelData;

    public UserLevelProvider() {
        levelData = new MutableLiveData<>();
        levelData.setValue(1);
    }

    public LiveData<Integer> getData(){
        return levelData;
    }

    public int getLevel() {
        return 1;
    }

    public static UserLevelProvider getInstance() {
        return INSTANCE;
    }
}
