package com.app.game.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.app.game.ui.drawables.Gradient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class GradientLayout extends RelativeLayout{

    private Gradient gradient;

    public GradientLayout(@NonNull Context context) {
        super(context);
        init(context, null);
    }

    public GradientLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public GradientLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public GradientLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        gradient = new Gradient(context, attrs);
        setBackground(gradient);
    }
}
