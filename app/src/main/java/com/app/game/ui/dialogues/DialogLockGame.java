package com.app.game.ui.dialogues;

import android.content.Context;

import com.app.game.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DialogLockGame extends AppDialog{

    public DialogLockGame(@NonNull Context context) {
        super(context);
    }

    public DialogLockGame(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected DialogLockGame(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    public int getLayout() {
        return R.layout.dialog_progress;
    }

}
