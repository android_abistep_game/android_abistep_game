package com.app.game.ui.dialogues;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class AppDialog extends Dialog{

    public AppDialog(@NonNull Context context) {
        super(context);
    }

    public AppDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected AppDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(getLayout());
    }

    public int calcHeight(float scrRate, int maxHeightDP, int minHeightDp) {
        int scrHeight = getContext().getResources().getDisplayMetrics().heightPixels;
        int maxHeight = (int) (getContext().getResources().getDisplayMetrics().density*maxHeightDP);
        int minHeight = (int) (getContext().getResources().getDisplayMetrics().density*minHeightDp);
        int ratedHeight = (int) (scrRate*scrHeight);
        maxHeight = Math.min(maxHeight, scrHeight);
        int height = Math.max(ratedHeight, minHeight);
        height = Math.min(height, maxHeight);
        return height;
    }

    public int calcWidth(float scrRate, int maxWidthDP, int minWidthDp) {
        int scrWidth = getContext().getResources().getDisplayMetrics().widthPixels;
        int maxWidth = (int) (getContext().getResources().getDisplayMetrics().density*maxWidthDP);
        int minWidth = (int) (getContext().getResources().getDisplayMetrics().density*minWidthDp);
        int ratedWidth = (int) (scrRate*scrWidth);
        maxWidth = Math.min(maxWidth, scrWidth);
        int width = Math.max(ratedWidth, minWidth);
        width = Math.min(width, maxWidth);
        return width;
    }

    public abstract int getLayout();
}
