package com.app.game.ui.drawables;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.app.game.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Gradient extends Drawable{

    private Path path;
    private Paint paint;
    private Shader shader;

    private float radTopLeft;
    private float radTopRight;
    private float radBottomLeft;
    private float radBottomRight;

    private boolean vertical;

    private List<Float> points;
    private List<Integer> colors;

    private int[] arrColors;
    private float[] arrPoints;

    public Gradient() {

        radTopLeft = 0;
        radTopRight = 0;
        radBottomLeft = 0;
        radBottomRight = 0;

        vertical = false;

        points = new ArrayList<>();
        colors = new ArrayList<>();

        path = new Path();
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    }

    public Gradient(Context context, AttributeSet attrs) {
        this();

        int[] colors = new int[10];
        float[] points = new float[10];

        if(attrs!=null) {

            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.AppGradient);

            radTopLeft = ta.getDimensionPixelSize(R.styleable.AppGradient_gradCornerTopLeft, 0);
            radTopRight = ta.getDimensionPixelSize(R.styleable.AppGradient_gradCornerTopRight, 0);
            radBottomLeft = ta.getDimensionPixelSize(R.styleable.AppGradient_gradCornerBottomLeft, 0);
            radBottomRight = ta.getDimensionPixelSize(R.styleable.AppGradient_gradCornerBottomRight, 0);

            vertical = ta.getBoolean(R.styleable.AppGradient_gradVertical, false);

            colors[0] = ta.getColor(R.styleable.AppGradient_gradClr0, 0);
            colors[1] = ta.getColor(R.styleable.AppGradient_gradClr1, 0);
            colors[2] = ta.getColor(R.styleable.AppGradient_gradClr2, 0);
            colors[3] = ta.getColor(R.styleable.AppGradient_gradClr3, 0);
            colors[4] = ta.getColor(R.styleable.AppGradient_gradClr4, 0);
            colors[5] = ta.getColor(R.styleable.AppGradient_gradClr5, 0);
            colors[6] = ta.getColor(R.styleable.AppGradient_gradClr6, 0);
            colors[7] = ta.getColor(R.styleable.AppGradient_gradClr7, 0);
            colors[8] = ta.getColor(R.styleable.AppGradient_gradClr8, 0);
            colors[9] = ta.getColor(R.styleable.AppGradient_gradClr9, 0);

            points[0] = ta.getFloat(R.styleable.AppGradient_gradPos0, -1);
            points[1] = ta.getFloat(R.styleable.AppGradient_gradPos1, -1);
            points[2] = ta.getFloat(R.styleable.AppGradient_gradPos2, -1);
            points[3] = ta.getFloat(R.styleable.AppGradient_gradPos3, -1);
            points[4] = ta.getFloat(R.styleable.AppGradient_gradPos4, -1);
            points[5] = ta.getFloat(R.styleable.AppGradient_gradPos5, -1);
            points[6] = ta.getFloat(R.styleable.AppGradient_gradPos6, -1);
            points[7] = ta.getFloat(R.styleable.AppGradient_gradPos7, -1);
            points[8] = ta.getFloat(R.styleable.AppGradient_gradPos8, -1);
            points[9] = ta.getFloat(R.styleable.AppGradient_gradPos9, -1);

            ta.recycle();
        }

        setVertical(vertical);

        for(int i = 0; i<colors.length; i++) {
            if(colors[i]!=0) {
                addPoint(colors[i], points[i]);
            } else if(i<2) {
                addPoint(Color.TRANSPARENT, -1);
            } else {
                break;
            }
        }
    }

    @Override
    public void draw(@NonNull Canvas canvas) {

        listsToArrays();

        int width = canvas.getWidth();
        int height = canvas.getHeight();

        float dTopLeft = 2*radTopLeft;
        float dTopRight = 2*radTopRight;
        float dBottomLeft = 2*radBottomLeft;
        float dBottomRight = 2*radBottomRight;

        path.reset();

        if(radBottomLeft>0.01f) {
            path.moveTo(radBottomLeft, height);
            path.arcTo(0, height-dBottomLeft, dBottomLeft, height, 90, 90, false);
        } else {
            path.moveTo(0, height);
        }

        if(radTopLeft>0.01f) {
            path.lineTo(0, radTopLeft);
            path.arcTo(0, 0, dTopLeft, dTopLeft, 180, 90, false);
        } else {
            path.lineTo(0, 0);
        }

        if(radTopRight>0.01f) {
            path.lineTo(width-radTopRight, 0);
            path.arcTo(width-dTopRight, 0, width, dTopRight, 270, 90, false);
        } else {
            path.lineTo(width, 0);
        }

        if(radBottomRight>0.01f) {
            path.lineTo(width, radBottomRight);
            path.arcTo(width-dBottomRight, height-dBottomRight, width, height, 0, 90, false);
        } else {
            path.lineTo(width, height);
        }

        path.close();

        shader = !vertical ? new LinearGradient(0, 0, width, 0, arrColors, arrPoints, Shader.TileMode.CLAMP) :
                new LinearGradient(0, 0, 0, height, arrColors, arrPoints, Shader.TileMode.CLAMP);
        paint.setShader(shader);

        canvas.drawPath(path, paint);
    }

    @Override
    public void setAlpha(int i) {

    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSPARENT;
    }

    public void setCornerTopLeft(int radius) {
        radTopLeft = radius;
    }

    public void setCornerTopRight(int radius) {
        radTopRight = radius;
    }

    public void setCornerBottomLeft(int radius) {
        radBottomLeft = radius;
    }

    public void setCornerBottomRight(int radius) {
        radBottomRight = radius;
    }

    public void setVertical(boolean vertical) {
        this.vertical = vertical;
    }

    public void listsToArrays() {

        arrPoints = new float[points.size()];
        for(int i = 0; i<arrPoints.length; i++) {
            if(points.get(i)<0) {
                arrPoints[i] = i*1.f/arrPoints.length;
            } else {
                arrPoints[i] = points.get(i);
            }
        }

        arrColors = new int[colors.size()];
        for(int i = 0; i<arrColors.length; i++) {
            arrColors[i] = colors.get(i);
        }
    }

    public void addPoint(int color, float position) {
        colors.add(color);
        points.add(position);
    }
}
