package com.app.game.ui.ctrls;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.app.game.R;
import com.app.game.ui.ActivityMain;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CtrlGame{

    @BindView(R.id.iLevels)
    protected View iLevels;
    @BindView(R.id.iBottomBar)
    protected View iBottomBar;
    @BindView(R.id.wvGame)
    protected WebView wvGame;

    private ActivityMain activity;

    private boolean modeGame;
    private boolean modeLoading;

    public CtrlGame(ActivityMain activity) {
        this.activity = activity;
        ButterKnife.bind(this, activity);

        modeGame = false;

        wvGame.setBackgroundColor(Color.BLACK);
        wvGame.getSettings().setJavaScriptEnabled(true);
        wvGame.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if(modeGame) {
                    modeLoading = true;
                    wvGame.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if(modeGame) {
                    modeLoading = false;
                    wvGame.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void start(String url) {

        url = url.startsWith("http") ? url : "https://"+url;

        modeGame = true;

        iLevels.setVisibility(View.GONE);
        iBottomBar.setVisibility(View.GONE);
        wvGame.setVisibility(View.INVISIBLE);
        wvGame.loadUrl(url);
    }

    public void cancel() {

        modeGame = false;
        modeLoading = true;

        iLevels.setVisibility(View.VISIBLE);
        iBottomBar.setVisibility(View.VISIBLE);
        wvGame.setVisibility(View.GONE);
        wvGame.loadUrl("");
    }

    public boolean onBackPressed() {

        if(modeGame) {
            cancel();
            return false;
        }

        return true;
    }


}
