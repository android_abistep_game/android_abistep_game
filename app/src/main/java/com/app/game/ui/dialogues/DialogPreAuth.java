package com.app.game.ui.dialogues;

import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;

import com.app.game.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DialogPreAuth extends AppDialog{

    @BindView(R.id.ivBtnClose)
    protected ImageView ivBtnClose;

    public DialogPreAuth(@NonNull Context context) {
        super(context);
    }

    public DialogPreAuth(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected DialogPreAuth(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        //findViewById(R.id.tvTitle).setBackground(new Gradient());
    }

    @Override
    public int getLayout() {
        return R.layout.dialog_gifts;
    }

    @OnClick(R.id.ivBtnClose)
    public void onViewClicked() {
        hide();
    }
}
