package com.app.game.ui.dialogues;

import android.content.Context;

import com.app.game.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DialogLevelUp extends AppDialog{

    public DialogLevelUp(@NonNull Context context) {
        super(context);
    }

    public DialogLevelUp(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected DialogLevelUp(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    public int getLayout() {
        return R.layout.dialog_progress;
    }

}
