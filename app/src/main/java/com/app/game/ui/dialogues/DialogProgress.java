package com.app.game.ui.dialogues;

import android.content.Context;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.game.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogProgress extends AppDialog{

    @BindView(R.id.pbProgress)
    protected ProgressBar pbProgress;
    @BindView(R.id.tvProgress)
    protected TextView tvProgress;

    public DialogProgress(@NonNull Context context) {
        super(context);
    }

    public DialogProgress(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected DialogProgress(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setCancelable(false);
    }

    @Override
    public int getLayout() {
        return R.layout.dialog_progress;
    }

    public void setMessage(String message){
        tvProgress.setText(message);
    }

    public void setProgress(float progress){
        String message = String.format("%.0f", progress*100);
        tvProgress.setText(message+"%");
    }

}
