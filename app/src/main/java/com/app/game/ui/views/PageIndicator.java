package com.app.game.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import com.app.game.R;

import androidx.annotation.Nullable;

public class PageIndicator extends View{

    private float itemSize;
    private float itemsDistance;
    private Drawable drawableSelected;
    private Drawable drawableUnselected;

    private int count;
    private int selected;

    public PageIndicator(Context context) {
        super(context);
        init(context, null);
    }

    public PageIndicator(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public PageIndicator(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public PageIndicator(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {

        if(attrs!=null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.AppPageIndicator);

            itemSize = ta.getDimensionPixelSize(R.styleable.AppPageIndicator_piItemSize, 0);
            itemsDistance = ta.getDimensionPixelSize(R.styleable.AppPageIndicator_piItemsDistance, 0);

            drawableSelected = ta.getDrawable(R.styleable.AppPageIndicator_piDrawableSelected);
            drawableUnselected = ta.getDrawable(R.styleable.AppPageIndicator_piDrawableUnselected);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(drawableSelected!=null && drawableUnselected!=null) {

            int top = (int) (0.5f*(canvas.getHeight()-itemSize));
            int bottom = (int) (top+itemSize);
            float totalWidth = count*itemSize+(count-1)*itemsDistance;
            float startX = 0.5f*(canvas.getWidth()-totalWidth);

            for(int i = 0; i<count; i++) {

                Drawable drawable = i==selected ? drawableSelected : drawableUnselected;

                int left = (int) (startX+i*itemSize+i*itemsDistance);
                int right = (int) (left+itemSize);

                drawable.setBounds(left, top, right, bottom);
                drawable.draw(canvas);
            }
        }
    }

    public void setCount(int count) {
        this.count = count;
        invalidate();
    }

    public void setSelected(int position) {
        this.selected = position;
        invalidate();
    }
}
