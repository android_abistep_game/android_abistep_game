package com.app.game.ui.ctrls;

import com.app.game.providers.LoadingSate;
import com.app.game.providers.games.GamesProvider;
import com.app.game.ui.ActivityMain;
import com.app.game.ui.dialogues.DialogProgress;

public class CtrlPreloading{

    private ActivityMain activity;
    private DialogProgress dialogProgress;

    private float progress;

    public CtrlPreloading(ActivityMain activity) {
        this.activity = activity;

        progress = 0;

        GamesProvider.getInstance().getData().observe(this.activity, data->{
            if(data.state==LoadingSate.NULL || data.state==LoadingSate.LOADING) {
                if(dialogProgress==null) {
                    dialogProgress = new DialogProgress(this.activity);
                    dialogProgress.show();
                    dialogProgress.setProgress(0.5f);
                }
            } else {
                if(dialogProgress!=null) {
                    dialogProgress.dismiss();
                    dialogProgress = null;
                }
            }
        });
    }

}
