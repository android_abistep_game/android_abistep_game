package com.app.game.ui.ctrls;

import android.content.Intent;
import android.widget.TextView;

import com.app.game.R;
import com.app.game.providers.user.AuthProvider;
import com.app.game.ui.ActivityMain;
import com.facebook.login.widget.LoginButton;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CtrlAuth{

    @BindView(R.id.lbFacebookLogin)
    protected LoginButton lbFacebookLogin;
    @BindView(R.id.tvBtnCollectBonus)
    protected TextView tvBtnCollectBonus;

    private ActivityMain activity;

    public CtrlAuth(ActivityMain activity) {
        this.activity = activity;
        ButterKnife.bind(this, activity);

        tvBtnCollectBonus.setOnClickListener(view->{
            /*if(AuthProvider.getInstance().isAuth())*/ {
                lbFacebookLogin.performClick();
            }/* else {
            }*/
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        AuthProvider.getInstance().onActivityResult(requestCode, resultCode, data);
    }

}
