package com.app.game.ui.views;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.app.game.utils.Utils;

public class GamesLayout extends ViewGroup{

    public static final int DEFAULT_ROWS = 2;
    public static final int DEFAULT_COLUMNS = 4;
    public static final int MAX_ITEM_SIZE_DP = 100;
    public static final float ITEMS_RELATIVE_DISTANCE = 0.35f;

    private int rows;
    private int columns;

    public GamesLayout(Context context) {
        super(context);
        init(context, null);
    }

    public GamesLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public GamesLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public GamesLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        rows = DEFAULT_ROWS;
        columns = DEFAULT_COLUMNS;
    }

    public void setTableParams(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        forceLayout();
        invalidate();
    }

    @Override
    public void addView(View child) {
        if(getChildCount()<rows*columns) {
            super.addView(child);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        float width = MeasureSpec.getSize(widthMeasureSpec);
        float height = MeasureSpec.getSize(heightMeasureSpec);
        float childSize = calcChildSize(width, height);

        int children = getChildCount();
        for(int i = 0; i<children; i++) {
            int childWidthSpec = MeasureSpec.makeMeasureSpec((int) childSize, MeasureSpec.EXACTLY);
            int childHeightSpec = MeasureSpec.makeMeasureSpec((int) childSize, MeasureSpec.EXACTLY);
            getChildAt(i).measure(childWidthSpec, childHeightSpec);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        float width = getWidth();
        float height = getHeight();

        float childSize = calcChildSize(width, height);
        float rowHeight = (int) (1.f*height/rows);
        float columnsDistance = (width-columns*childSize)/(columns-1);
        columnsDistance = Math.min(columnsDistance, ITEMS_RELATIVE_DISTANCE*childSize);
        columnsDistance = Math.max(columnsDistance, 0);

        int children = Math.min(getChildCount(), rows*columns);
        for(int i = 0, row = 0, column = 0; i<children; i++, column++) {

            float top;
            float left;
            float right;
            float bottom;

            if(column==columns) {
                row++;
                column = 0;
            }

            if(row==0) {
                top = rowHeight-childSize;
            } else if(row==rows-1) {
                top = height-rowHeight;
            } else {
                float topRow = rowHeight*row;
                top = topRow+0.5f*(rowHeight-childSize);
            }

            float col0X = 0.5f*(width-columns*childSize-(columns-1)*columnsDistance);
            left = col0X+column*childSize+column*columnsDistance;

            right = left+childSize;
            bottom = top+childSize;

            getChildAt(i).layout((int) left, (int) top, (int) right, (int) bottom);
        }

    }

    public float calcChildSize(float layoutWidth, float layoutHeight) {

        float maxItemHeight = 1.f*layoutHeight/rows;
        float maxItemWidth = 1.f*layoutWidth/columns;
        float maxItemSize = Utils.dpToPx(getContext().getResources(), MAX_ITEM_SIZE_DP);

        float itemSize = Math.min(maxItemWidth, maxItemHeight);
        itemSize = Math.min(itemSize, maxItemSize);

        return itemSize;
    }

    public static int calcMaxImagesSize(Context context, int rows, int columns) {

        Resources resources = context.getResources();

        float maxItemHeight = context.getResources().getDisplayMetrics().heightPixels/rows;
        float maxItemWidth = context.getResources().getDisplayMetrics().widthPixels/columns;
        float maxItemSize = Utils.dpToPx(resources, MAX_ITEM_SIZE_DP);

        float itemSize = Math.min(maxItemWidth, maxItemHeight);
        itemSize = Math.min(itemSize, maxItemSize);

        return (int) itemSize;
    }

}
