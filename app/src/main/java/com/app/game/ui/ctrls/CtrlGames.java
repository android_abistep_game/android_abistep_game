package com.app.game.ui.ctrls;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.game.R;
import com.app.game.models.Room;
import com.app.game.providers.LoadingSate;
import com.app.game.providers.games.GamesProvider;
import com.app.game.ui.ActivityMain;
import com.app.game.ui.views.GameView;
import com.app.game.ui.views.GamesLayout;
import com.app.game.ui.views.PageIndicator;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CtrlGames{

    public static final int ROWS = 2;

    @BindView(R.id.iLevels)
    protected View iLevels;
    @BindView(R.id.vpLevels)
    protected ViewPager2 vpLevels;
    @BindView(R.id.tvLevels)
    protected TextView tvLevels;
    @BindView(R.id.piIndicator)
    protected PageIndicator piIndicator;

    @BindString(R.string.levels)
    protected String baseRoomTitle;

    private ActivityMain activity;

    private List<String> roomTitles;

    public CtrlGames(ActivityMain activity) {
        this.activity = activity;
        ButterKnife.bind(this, activity);

        roomTitles = new ArrayList<>();

        GamesProvider.getInstance().getData().observe(this.activity, data->{
            if(data.state==LoadingSate.SUCCESS) {

                roomTitles.clear();
                for(int i = 0, startLevel = 1; i<data.rooms.size(); i++) {
                    int lastLevel = startLevel+data.rooms.get(i).games.size()-1;
                    String roomTitle = baseRoomTitle+" "+startLevel+" - "+lastLevel;
                    startLevel = lastLevel+1;
                    roomTitles.add(roomTitle);
                }

                vpLevels.setAdapter(new Adapter(this.activity, data.rooms));
                piIndicator.setCount(data.rooms.size());
                piIndicator.setSelected(0);
            }
        });

        vpLevels.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback(){

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                piIndicator.setSelected(position);
                tvLevels.setText(roomTitles.get(position));
            }

        });
    }

    protected static class Adapter extends RecyclerView.Adapter<ViewHolder>{

        private List<Room> rooms;
        private LayoutInflater inflater;

        public Adapter(Context context, List<Room> rooms) {
            inflater = LayoutInflater.from(context);
            this.rooms = rooms;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.page_levels, parent, false);
            ViewHolder holder = new ViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.onBind(rooms.get(position));
        }

        @Override
        public int getItemCount() {
            return rooms==null ? 0 : rooms.size();
        }
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder{

        private static int minColumns = 1;
        private static int maxImageSize = -1;

        @BindView(R.id.lGames)
        protected GamesLayout lGames;

        private Context context;

        private List<GameView> gameViews = new ArrayList<>();

        public ViewHolder(@NonNull View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        public void onBind(Room room) {

            int columns = Math.max(room.games.size()/ROWS, minColumns);
            minColumns = columns;

            lGames.setTableParams(ROWS, columns);

            if(maxImageSize<=0) {
                maxImageSize = GamesLayout.calcMaxImagesSize(context, ROWS, columns);
            }

            gameViews.clear();
            lGames.removeAllViews();
            for(int i = 0; i<room.games.size(); i++) {
                GameView gameView = new GameView(context);
                gameViews.add(gameView);
                lGames.addView(gameView);
                gameView.setGame(room.games.get(i), maxImageSize);
            }
        }
    }

}
