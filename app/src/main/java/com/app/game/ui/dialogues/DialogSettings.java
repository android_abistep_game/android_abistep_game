package com.app.game.ui.dialogues;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.game.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DialogSettings extends AppDialog{

    @BindView(R.id.tvBtnDone)
    protected TextView tvBtnDone;
    @BindView(R.id.ivBtnClose)
    protected ImageView ivBtnClose;

    public DialogSettings(@NonNull Context context) {
        super(context);
    }

    public DialogSettings(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected DialogSettings(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    public int getLayout() {
        return R.layout.dialog_settings;
    }

    @Override
    public void show() {
        super.show();
        int width = calcWidth(0.65f, 640, 480);
        int height = calcHeight(0.8f, 320, 300);
        getWindow().setLayout(width, height);
    }

    public static DialogSettings show(Context context) {
        DialogSettings dialog = new DialogSettings(context);
        dialog.show();
        return dialog;
    }

    @OnClick({R.id.tvBtnDone, R.id.ivBtnClose})
    public void onViewClicked(View view) {
        switch(view.getId()) {
            case R.id.tvBtnDone:
                dismiss();
                break;
            case R.id.ivBtnClose:
                dismiss();
                break;
        }
    }
}
