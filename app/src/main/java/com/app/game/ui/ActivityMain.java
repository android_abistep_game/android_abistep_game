package com.app.game.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.app.game.R;
import com.app.game.models.Game;
import com.app.game.providers.user.AuthProvider;
import com.app.game.providers.games.GamesProvider;
import com.app.game.ui.ctrls.CtrlAuth;
import com.app.game.ui.ctrls.CtrlCoins;
import com.app.game.ui.ctrls.CtrlGame;
import com.app.game.ui.ctrls.CtrlGames;
import com.app.game.ui.ctrls.CtrlOptions;
import com.app.game.ui.ctrls.CtrlPreloading;
import com.app.game.ui.ctrls.CtrlPurchases;
import com.app.game.ui.dialogues.DialogGifs;
import com.app.game.ui.dialogues.DialogPurchases;
import com.app.game.ui.dialogues.DialogSettings;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityMain extends AppCompatActivity{

    @BindView(R.id.tvBtnGifts)
    protected TextView tvBtnGifts;
    @BindView(R.id.tvBuyCoins)
    protected TextView tvBuyCoins;
    @BindView(R.id.tvBtnOptions)
    protected TextView tvBtnOptions;

    private CtrlAuth ctrlAuth;
    private CtrlCoins ctrlCoins;
    private CtrlGame ctrlGame;
    private CtrlGames ctrlGames;
    private CtrlOptions ctrlOptions;
    private CtrlPurchases ctrlPurchases;
    private CtrlPreloading ctrlPreloading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        //hideSystemNavigationBar();

        GamesProvider.getInstance().init(this);
        GamesProvider.getInstance().refersh();

        AuthProvider.getInstance().init();
        AuthProvider.getInstance().log();

        ctrlAuth = new CtrlAuth(this);
        ctrlCoins = new CtrlCoins();
        ctrlGame = new CtrlGame(this);
        ctrlGames = new CtrlGames(this);
        ctrlOptions = new CtrlOptions();
        ctrlPurchases = new CtrlPurchases();
        ctrlPreloading = new CtrlPreloading(this);
    }

    @OnClick({R.id.tvBtnGifts, R.id.tvBuyCoins, R.id.tvBtnOptions, R.id.tvBtnTbBuyCoins})
    public void onViewClicked(View view) {
        switch(view.getId()) {
            case R.id.tvBtnGifts:
                new DialogGifs(this).show();
                break;
            case R.id.tvBuyCoins:
            case R.id.tvBtnTbBuyCoins:
                new DialogPurchases(this).show();
                break;
            case R.id.tvBtnOptions:
                DialogSettings.show(this);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if(ctrlGame.onBackPressed()) {
            super.onBackPressed();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        ctrlAuth.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void startGame(Game game){
        ctrlGame.start(game.url);
    }

    private void hideSystemNavigationBar() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION|View.SYSTEM_UI_FLAG_FULLSCREEN);
    }
}
