package com.app.game.ui.views;

import android.content.Context;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.app.game.R;
import com.app.game.models.Game;
import com.app.game.providers.user.UserLevelProvider;
import com.app.game.ui.ActivityMain;
import com.app.game.utils.Utils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GameView extends RelativeLayout implements View.OnTouchListener{

    private static final float TOUCH_SCALE = 0.8f;
    private static final float[] LOCK_FILTER = new float[]{
            0.20f, 0.20f, 0.20f, 0, 0,
            0.20f, 0.20f, 0.20f, 0, 0,
            0.20f, 0.20f, 0.20f, 0, 0,
            0, 0, 0, 1, 0};

    @BindView(R.id.ivGame)
    protected ImageView ivGame;
    @BindView(R.id.ivlock)
    protected ImageView ivlock;

    private ColorFilter colorFilterLocked;
    private ColorFilter colorFilterUnlocked;

    private Game game;
    private int minLevel;
    private int maxImageSize;

    public GameView(Context context) {
        super(context);
        init(context, null);
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public GameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public GameView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_game, this, false);
        ButterKnife.bind(this, view);
        addView(view);

        ivGame.setOnTouchListener(this);

        colorFilterUnlocked = ivGame.getColorFilter();
        colorFilterLocked = new ColorMatrixColorFilter(new ColorMatrix(LOCK_FILTER));
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        int action = event.getAction();

        switch(action) {

            case MotionEvent.ACTION_DOWN:
                if(UserLevelProvider.getInstance().getLevel() >= minLevel) {
                    ivGame.setScaleX(TOUCH_SCALE);
                    ivGame.setScaleY(TOUCH_SCALE);
                }
                break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                ivGame.setScaleX(1);
                ivGame.setScaleY(1);
                break;
        }

        return false;
    }

    public void setGame(Game game, int maxImageSize) {

        this.game = game;
        this.maxImageSize = maxImageSize;

        ivGame.setColorFilter(colorFilterUnlocked);

        Picasso.get()
                .load(game.imgLogo)
                .resize(maxImageSize, maxImageSize)
                .onlyScaleDown()
                .into(ivGame);

        minLevel = Utils.parseInt(game.accessMinLevel, 1000);
        if(UserLevelProvider.getInstance().getLevel() >= minLevel) {
            ivGame.setColorFilter(colorFilterUnlocked);
            ivlock.setVisibility(GONE);
            ivGame.setOnClickListener(view->((ActivityMain) getContext()).startGame(this.game));
        } else {
            ivGame.setColorFilter(colorFilterLocked);
            ivlock.setVisibility(VISIBLE);
            ivGame.setOnClickListener(null);
        }
    }

    public void refresh() {
        if(game!=null) {
            setGame(game, maxImageSize);
        }
    }
}
